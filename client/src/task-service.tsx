import axios from "axios";

axios.defaults.baseURL = "http://localhost:3000/api/v2";

export type Task = {
  id: number;
  title: string;
  done: boolean;
};
type Response_data = {
  stdoutData: "";
  stderrData: "";
};

class TaskService {
  async create(input: string, language: string) {
    try {
      const response = await axios.post<Response_data>("/tasks", {
        input: input,
        language: language,
      });
      return response.data;
    } catch (error) {
      throw error;
    }
  }
}

const taskService = new TaskService();
export default taskService;
