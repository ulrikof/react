import * as React from "react";
import { createRoot } from "react-dom/client";
import { Component } from "react-simplified";
import { HashRouter, Route } from "react-router-dom";
import { NavBar, Card, Alert, Row, Column, Form, Button } from "./widgets";
import taskService from "./task-service";

class App extends Component {
  input: string = "";
  stdout: string = "";
  stderr: string = "";
  language = "js";

  render() {
    return (
      <>
        <Card title="Skriv inne kode!">
          <Row>
            <Column>
              <select
                value={this.language}
                onChange={(event) => (this.language = event.target.value)}
              >
                <option value="js">js</option>
                <option value="python">python</option>
              </select>
              <Form.Textarea
                type="string"
                value={this.input}
                onChange={(event) => (this.input = event.currentTarget.value)}
              ></Form.Textarea>
              <Button.Success
                onClick={() => {
                  this.runButton();
                }}
              >
                Run Code
              </Button.Success>
            </Column>
          </Row>
        </Card>
        <Card title="Output">
          <Card title="Stdout">
            <pre>{this.stdout}</pre>
          </Card>
          <Card title="Stderr">
            <pre>{this.stderr}</pre>
          </Card>
        </Card>
      </>
    );
  }

  async runButton() {
    try {
      let response = await taskService.create(this.input, this.language);
      this.stdout = response.stdoutData;
      this.stderr = response.stderrData;
    } catch (error) {
      console.error(error);
    }
  }
}

let root = document.getElementById("root");
if (root)
  createRoot(root).render(
    <>
      <App></App>
    </>
  );
