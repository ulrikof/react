import express from "express";
import { spawn } from "node:child_process";

const router = express.Router();

router.post("/tasks", (request, response) => {
  const data = request.body.input;
  const language = request.body.language;
  let setup;

  if (language === "python") {
    setup = ["run", "--rm", "python:3.8", "python", "-c", `${data}`];
  } else if (language === "js") {
    setup = ["run", "--rm", "node:latest", "node", "-e", `${data}`];
  }

  const ls = spawn("docker", setup);

  let response_data = {
    stdoutData: "",
    stderrData: "",
  };
  ls.stdout.on("data", (data: string) => {
    response_data.stdoutData += data;
  });

  ls.stderr.on("data", (data: string) => {
    response_data.stderrData += data;
  });

  ls.on("close", (exit_status: number) => {
    response.send(response_data);
  });
});
export default router;
